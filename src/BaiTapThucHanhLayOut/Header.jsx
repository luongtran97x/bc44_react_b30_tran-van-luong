import React, { Component } from "react";

export default class Header extends Component {
  render() {
    return (
      <div className="bg-dark font-weight-bold">
        <ul className="nav d-flex justify-content-around ">
          <div className="nav-left py-3 ">
            <li className="nav-item ">
              <a className="nav-link active text-white h4" href="#">
                Start Bootstrap
              </a>
            </li>
          </div>
          <div className="nav-right d-flex py-3">
            <li className="nav-item ">
              <a className="nav-link text-white" href="#" >
                Home
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link text-white" href="#">
                About
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link text-white" href="#">
                Services
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link text-white" href="#">
                Contact
              </a>
            </li>
          </div>
        </ul>
      </div>
    );
  }
}
