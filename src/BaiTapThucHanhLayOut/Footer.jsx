import React, { Component } from 'react'

export default class Footer extends Component {
  render() {
    return (
      <div className="bg-secondary py-5">
        <div className="container text-center text-white ">
          <p>Copyright © Your Website 2023</p>
        </div>
      </div>
    );
  }
}
