import React, { Component } from "react";

export default class Item extends Component {
  render() {
    return (
      <div
        className="items row m-0  justify-content-center"
        style={{ gap: 32 }}
      >
        <div
          className="item card col-xs-12 col-sm-6  col-lg-3 p-0 mb-3"
          style={{ width: 300 }}
        >
          <div className="card-header bg-transparent">
            <img
              className="card-img-top"
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS03kU4dJIELF0cQ2-3RDOdddELLgszn2inLQ&usqp=CAU"
              alt="Card image cap"
              style={{ maxWidth: "100%", height: 250 }}
            />
          </div>
          <div className="card-body py-5 text-center">
            <h5 className="card-title ">Card title</h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
          </div>
          <div className="card-footer text-center">
            <a href="#" className="btn btn-primary">
              Find Out More!
            </a>
          </div>
        </div>
        <div
          className="item card col-xs-12 col-sm-6 col-lg-3 p-0 mb-3"
          style={{ width: 300 }}
        >
          <div className="card-header bg-transparent">
            <img
              className="card-img-top"
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS03kU4dJIELF0cQ2-3RDOdddELLgszn2inLQ&usqp=CAU"
              alt="Card image cap"
              style={{ maxWidth: "100%", height: 250 }}
            />
          </div>
          <div className="card-body py-5 text-center">
            <h5 className="card-title ">Card title</h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
          </div>
          <div className="card-footer text-center">
            <a href="#" className="btn btn-primary">
              Find Out More!
            </a>
          </div>
        </div>
        <div
          className="item card col-xs-12 col-sm-6 col-lg-3 p-0 mb-3"
          style={{ width: 300 }}
        >
          <div className="card-header bg-transparent">
            <img
              className="card-img-top"
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS03kU4dJIELF0cQ2-3RDOdddELLgszn2inLQ&usqp=CAU"
              alt="Card image cap"
              style={{ maxWidth: "100%", height: 250 }}
            />
          </div>
          <div className="card-body py-5 text-center">
            <h5 className="card-title ">Card title</h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
          </div>
          <div className="card-footer text-center">
            <a href="#" className="btn btn-primary">
              Find Out More!
            </a>
          </div>
        </div>
        <div
          className="item card col-xs-12 col-sm-6 col-lg-3 p-0 mb-3"
          style={{ width: 300 }}
        >
          <div className="card-header bg-transparent">
            <img
              className="card-img-top"
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS03kU4dJIELF0cQ2-3RDOdddELLgszn2inLQ&usqp=CAU"
              alt="Card image cap"
              style={{ maxWidth: "100%", height: 250 }}
            />
          </div>
          <div className="card-body py-5 text-center">
            <h5 className="card-title ">Card title</h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
          </div>
          <div className="card-footer text-center">
            <a href="#" className="btn btn-primary">
              Find Out More!
            </a>
          </div>
        </div>
      </div>
    );
  }
}
