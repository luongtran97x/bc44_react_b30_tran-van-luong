import React, { Component } from "react";

export default class Banner extends Component {
  render() {
    return (
      <div className="container py-5">
        <div className="body p-5 text-center rounded bg-light">
          <h1 className="font-weight-bold">A warm welcome!</h1>
          <p className="font-weight-normal h5 mb-3">
            Bootstrap utility classes are used to create this jumbotron since
            the old component has been removed from the framework. Why create
            custom CSS when you can use utilities?
          </p>
          <button className="bg-primary text-white px-3 py-2 rounded border-0 ">
            Call to action
          </button>
        </div>
      </div>
    );
  }
}
